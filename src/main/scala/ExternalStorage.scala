import java.io.RandomAccessFile
import java.nio.channels.FileChannel.MapMode._



/**
 * Created by Nikolay Filchenko on 1/22/15.
 */
class ExternalStorage[T <: ModifyMonitor](path : String)(implicit s : Serializer[T]) {
  val maxCacheSize = 10

  private var (file, buffer) = loadFile

  private var cache : Map[Int, T] = Map()
  private var cacheQueue : Seq[Int] = Seq()

  private var N = 0

  var reads = 0
  var writes = 0

  private def loadFile = {
    val file = new RandomAccessFile(path, "rw")
    file.setLength(0)
    val channel = file.getChannel
    (file, channel.map(READ_WRITE, 0, file.length))
  }

  private def update(i : Int, v : T): Unit = {
    if (v.isModified) {
      //println("Save block " + i)
      buffer.position(i * s.size)
      buffer.put(s.serialize(v))
      cache = cache - i
      cacheQueue = cacheQueue.filterNot(_ == i)
      writes = writes + 1
    }
  }

  def apply(i : Int) : T = {
    if (i >= N)
      throw new ArrayIndexOutOfBoundsException(i + ">" + N)
    if (cache.size > maxCacheSize)
      sync()

    if (!cache.contains(i)) {
      //println("Read block " + i)
      cacheQueue = cacheQueue :+ i
      buffer.position(i * s.size)
      val buf = Array.ofDim[Byte](s.size)
      buffer.get(buf)
      reads = reads + 1
      cache = cache + (i -> s.deserialize(buf))
      cache(i).setNotModified()
    }

    cacheQueue = cacheQueue.filterNot(_ == i) :+ i
    cache(i)
  }

  def length = N

  def isEmpty = length == 0

  def last = this(length - 1)

  def head = this(0)

  def append(v : T) = {
    N = N + 1
    if (N * s.size > file.length()) {
      //println("Alloc block " + (N - 1))
      sync()
      file.setLength(N * s.size)
      buffer = file.getChannel.map(READ_WRITE, 0, file.length)
      buffer.position(buffer.capacity() - s.size)
      buffer.put(s.serialize(v))
      writes = writes + 1
      v.setNotModified()
    }

    cache = cache + ((length - 1) -> v)
  }

  def removeLast() = {
    N = N - 1

    cache = cache - N
    cacheQueue = cacheQueue.filterNot(_ == N)
  }

  private def sync() = {
    cacheQueue.take(maxCacheSize / 2).foreach(x => update(x, cache(x)))
    cacheQueue = cacheQueue.drop(maxCacheSize / 2)
  }

  def syncAll() = {
    cacheQueue.foreach(x => update(x, cache(x)))
    cacheQueue = Seq()
  }
}

object ExternalStorage {
  def apply[T <: ModifyMonitor](path : String)(implicit s : Serializer[T]) = new ExternalStorage[T](path)
}