import java.nio.ByteBuffer

/**
 * Created by Nikolay Filchenko on 1/22/15.
 */
trait Serializer[T] {
  val size : Int
  def serialize(v : T) : Array[Byte]
  def deserialize(a : Array[Byte]) : T
}

object Serializer {
  implicit val LongSerializer = new Serializer[Long] {
    override val size: Int = 8

    override def serialize(v: Long): Array[Byte] = ByteBuffer.allocate(size).putLong(v).array

    override def deserialize(a: Array[Byte]): Long = ByteBuffer.wrap(a).getLong
  }
}


