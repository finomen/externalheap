/**
 * Created by finomen on 1/23/15.
 */
trait ModifyMonitor {
  def isModified : Boolean
  def setNotModified() : Unit
}
