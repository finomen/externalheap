import java.nio.ByteBuffer

import scala.reflect.ClassTag

/**
 * Created by Nikolay Filchenko on 1/20/15.
 */
class ExternalHeap[T](path : String)(implicit o: Ordering[T], t: ClassTag[T], s : Serializer[T]) {
  val nodeSize = 100

  implicit val nodeSerializer = new Serializer[MonitoredSet[T]] {
    override val size: Int = 4 + nodeSize * s.size

    override def serialize(v: MonitoredSet[T]): Array[Byte] = {
      val res = ByteBuffer.allocate(4).putInt(v.length).array() ++ v.data.toArray.flatMap(s.serialize)
      res
    }

    override def deserialize(a: Array[Byte]): MonitoredSet[T] = {
      val sz = ByteBuffer.wrap(a).getInt
      MonitoredSet[T](a.drop(4).grouped(s.size).take(sz).map(s.deserialize))
    }
  }

  val data = ExternalStorage[MonitoredSet[T]](path)

  def child(i: Int) = i * 2 + 1

  def parent(i: Int) = (i - 1) / 2

  def siftDown(p: Int): Unit = {
    if (child(p) >= data.length)
      return

    if (child(p) + 1 >= data.length || o.compare(data(child(p)).min, data(child(p) + 1).min) < 0) {
      merge(child(p), p)

      siftDown(child(p))
    } else {
      merge(child(p) + 1, p)
      siftDown(child(p) + 1)
    }


  }

  def siftup(p: Int): Unit = {
    if (p == 0)
      return
    if (o.compare(data(p).min, data(parent(p)).max) <= 0) {
      merge(p, parent(p))
      siftup(parent(p))
    }
  }


  //TODO: optimize
  def merge(l : Int, s : Int) = {
    val large = data(l)
    val small = data(s)

    while (o.compare(small.max, large.min) > 0) {
      small add large.extractMin
      large add small.extractMax
    }
  }

  def heapify(p: Int): Unit = {
    if (p == 0)
      if (child(p) + 1 <= parent(data.length - 1))
        heapify(child(p) + 1)
    if (child(p) <= parent(data.length - 1))
      heapify(child(p))
    siftDown(p)
  }

  def add(v: T): Unit = {
    if (data.isEmpty || data.last.length >= nodeSize) {
      data.append(MonitoredSet[T](Seq(v)))
    } else {
      data.syncAll()
      data.last add v
    }

    if (o.compare(data.last.min, data(parent(data.length - 1)).max) < 0)
      siftup(data.length - 1)
  }

  def add(v : Seq[T]): Unit = {
    if (data.isEmpty || data.last.length == nodeSize) {
      data.append(MonitoredSet[T]())
    }

    val cnt = nodeSize - data.last.length
    v.take(cnt).foreach(data.last.add)

    if (o.compare(data.last.min, data(parent(data.length - 1)).max) < 0)
      siftup(data.length - 1)

    if (cnt < v.length)
      add(v.drop(cnt))
  }


  def min = data.head.min

  def extractMin(): T = {

    data.syncAll()
    val res = data.head.extractMin()

    if (data.length > 1) {
      data.syncAll()
      data.head.add(data.last.extractMin())

      if (data.last.isEmpty)
        data.removeLast()

      siftDown(0)
    }

    res
  }

  def extractMin(len : Int): Seq[T] = {
    data.syncAll()
    var res : Seq[T] = Seq()

    while (res.length < len && !data.head.isEmpty)
      res = res :+ data.head.extractMin

    if (data.length > 1) {
      data.syncAll()

      while (data.head.length < nodeSize) {
        while (data.head.length < nodeSize && !data.last.isEmpty)
          data.head.add(data.last.extractMin())

        if (data.last.isEmpty)
          data.removeLast()
      }

      siftDown(0)
    }

    if (res.length < len)
      res ++ extractMin(len - res.length)
    else
      res
  }

  override def toString = toString(0)

  def toString(p: Int): String = {
    if (p >= data.length)
      ""
    else
      "{" + data(p) + toString(child(p)) + "," + toString(child(p) + 1) + "}"
  }

  def length = if (data.isEmpty) 0 else (data.length - 1) * nodeSize + data.last.length

  def isEmpty = length == 0
}
