import java.util
import java.util.Map.Entry

/**
 * Created by Nikolay Filchenko on 1/23/15.
 */
class MonitoredSet[T](implicit o : Ordering[T]) extends AnyRef with ModifyMonitor {
  private var modified = false

  private val set : util.TreeMap[T, List[T]] = new util.TreeMap[T, List[T]]()
  private var size : Int = 0

  override def isModified: Boolean = modified

  override def setNotModified(): Unit = modified = false

  def length = size
  def isEmpty = set.isEmpty

  private def remove(v : T) =
    if (set.get(v).length == 1)
      set remove v
    else
      set put(v, set.get(v).tail)


  def extractMin() = {
    size = size - 1
    modified = true
    val r = set.firstEntry.getValue.head
    remove(set.firstKey)
    r
  }

  def extractMax() = {
    size = size - 1
    val r = set.lastEntry.getValue.head
    modified = true
    remove(set.lastKey)
    r
  }

  def add(v : T) = {
    size = size + 1
    modified = true
    set put (v, set.getOrDefault(v, List()) :+ v)
  }

  def min = set.firstEntry.getValue.head

  def max = set.lastEntry.getValue.head

  def data = {
    val es = set.entrySet().toArray(Array[Entry[T, List[T]]]())
    es.map(x => x.getValue).foldLeft(List[T]())((l, r) => l ++ r)
  }

  override def toString = data.toString()
}

object MonitoredSet {
  def apply[T]()(implicit o : Ordering[T]) = new MonitoredSet[T]()

  //TODO: optimize
  def apply[T](s : Iterator[T])(implicit o : Ordering[T]) = {
    val r = new MonitoredSet[T]()
    s.foreach(r add)
    r
  }

  //TODO: optimize
  def apply[T](s : collection.Iterable[T])(implicit o : Ordering[T]) = {
    val r = new MonitoredSet[T]()
    s.foreach(r add)
    r
  }
}
