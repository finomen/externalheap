import scala.reflect.ClassTag

/**
 * Created by Nikolay Filchenko on 1/22/15.
 */
class CachedExternalHeap[T](path : String)(implicit o: Ordering[T], t: ClassTag[T], s : Serializer[T]) {
  val minCacheSize = 4096
  val maxCacheSize = minCacheSize * 2

  val heap = new ExternalHeap[T](path)
  val cache = MonitoredSet[T]()


  def add(v : T) = {
    cache add v
    updateCache()
  }

  def min = if (heap.isEmpty) cache.min else o.min(cache.min, heap.min)

  def extractMin() = {
    if (!heap.isEmpty && o.compare(cache.min, heap.min) > 0) {
      heap.extractMin(minCacheSize).foreach(cache.add)
    }
    val res = cache.extractMin()
    updateCache()
    res
  }

  def length = cache.length + heap.length

  def isEmpty = length == 0

  private def updateCache() = {
    if (cache.length > maxCacheSize) {
      var store: Seq[T] = Seq()
      while (cache.length > minCacheSize) {
        store = store :+ cache.extractMax
      }
      heap.add(store)
    } else if (cache.isEmpty) {
        heap.extractMin(Math.min(minCacheSize, heap.length)).foreach(cache.add)
    }
  }
}
