import scala.collection.mutable
import scala.util.Random

/**
 * Created by finomen on 1/20/15.
 */
object Main extends App {


  def MyOrdering = new Ordering[Long] {
    def compare(a: Long, b: Long) = if (a < b) 1 else if (a > b) -1 else 0
  }


  val cachedHeap = new CachedExternalHeap[Long]("cachedHeap")
  val heap = new ExternalHeap[Long]("heap")
  val pq = new mutable.PriorityQueue[Long]()(MyOrdering)
  println("Heap")
  val r = new Random()




  (0 until 1000000).foreach(f = x => {
    if (x % 10000 == 0) {
      println(x + " test passed. Reads: " + heap.data.reads + " Writes: " + heap.data.writes)
      println("Cached " + x + " test passed. Reads: " + cachedHeap.heap.data.reads + " Writes: " + cachedHeap.heap.data.writes)
    }
    if (x < 20000 || pq.size < 10000 || pq.isEmpty || r.nextBoolean()) {
      val l = r.nextLong() % 10
      pq.enqueue(l)
      heap.add(l)
      cachedHeap.add(l)
    } else {
      heap.extractMin()
      cachedHeap.extractMin()
      pq.dequeue()
    }

    if (pq.nonEmpty && pq.min != heap.min) {
      println("WTF???? " + heap.min + "!=" + pq.min)
      println(heap)
      println(cachedHeap)
      System.exit(-1)
    }

    if (pq.nonEmpty && pq.min != cachedHeap.min) {
      println("Cached WTF???? " + cachedHeap.min + "!=" + pq.min)
      println(heap)
      println(cachedHeap)
      System.exit(-1)
    }
  })


}
